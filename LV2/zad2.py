import numpy as np
import matplotlib.pyplot as plt

a=[]

for i in range(100):
    a.append(np.random.randint(1,7))

labels, counts = np.unique(a, return_counts=True)
plt.bar(labels, counts, align='center')
print (a)
plt.hist(a,bins=20)
plt.show()